Welcome to Snake!

Drag the file 'SNAKECOMPLETE' which is a .hex file into the micro:bit.

The player shall be greeted with 'snake' and then the game will begin.

Tilt the micro:bit in all directions to control the direction of the snake. 

Collecting 2 'chunks of food' will make the snake bigger but if the player touches the wall the game will end. 

Accumulate as many points as possible without touching the wall.

Once the player touches the wall the game will end with the message ' Game over! - Score: "score will be inserted here" '.

Once this message passes, a new game will start.

Enjoy!